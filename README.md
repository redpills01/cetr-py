# Implementation of Content Extraction via Tag Ratios (CETR) using Python

### Overview
********
The field of content extraction, within the larger purview of data mining and information retrieval, is primarily concerned with the identification of the main text of a document, such as a Web page or Web site. There exists a handful of methods to extract main content from a web page such as  

- Content Extraction via Tag Ratio ( CETR )  
- Wrapper Induction 
- Boilerpipe 

This is a humble attempt from `red_pills` to implement [CETR - Content Extraction via Tag Ratios](http://www3.nd.edu/~tweninge/cetr/). 

### CETR
********
Most of the websites that are developed by humans typically contain many lines of HTML code. Some of these lines are content and the residue is ads, navigational menu, copyright information which are generally considered non-content. If we observe the ratio of non-HTML characters to the number of HTML tags in the line, we see that the text-to-tag ratio (TTR) scores are higher for content lines and lower for non-content lines because non-content areas have very little text and lots of formatting. Thus we see a large jump in TTR ratio for content areas. This can be seen from the derivative which peaks at the beginning and end of content sections. The TTR curve is smoothened before taking derivatives to weed out irregularities in a web page. Finally, we use K-Means Clustering to cluster the line numbers by using Smoothed TTR array and Derivative of Smoothed TTR array as features. Normally, the points which are farther away from the origin is chosen as content and points closer to the origin as non-content.

### Tools Used
**************
- Selenium/BeautifulSoup (For Web Extraction)
- Scikit Learn (For K-Means Clustering)
- Bokeh (For Interactive Plotting)
- Numpy (For scientific computation)
- Python Tornado (For backend)
- Bootstrap v3.3.7 (For responsive frontend) 

### Workflow
********
1. First, the HTML content is extracted from the web page using either Selenium/BeautifulSoup.
2. HTML string is prettified and split to lines.
2. TTR calculator computes the TTR for each line.
3. The resultant TTR array is smoothened through Gaussian smoothing.
4. The derivative is calculated for smoothened TTR array with respect to line numbers.
    - This will remove the unwanted noise and rapid phenomena. 
5. Line numbers are clustered via K-Means to separate out content from non-content.
    - K-Means use TTR and Derivative of TTR as features.

### Challenges
************
- Sometimes many lines of HTML code coalesce into a single line and the need of the hour to separate them.
- Choosing the value of K for clustering.
    - The text content varies from site to site, we can't pinpoint the number of clusters in a particular site. This node requires further research.  
    - Here we've taken three clusters and consider farthest two from origin as relevent content clusters
- Rendering the final clustered HTML content.
    - Since we have high chances of removal of content in-between parent tags, the resulting HTML may not be properly rendered.

### How to use ? 
***
- Clone the project.
```
git clone https://bitbucket.org/red_pills/cetr-py.git
```
- Run the server.
```python
python web-app.py
```
- Open http://localhost:8080/cetr in a browser
- Enter a URL in the input box and click Run.  
- Optionally choose a scraper engine either BeautifulSoup(default) or Selenium.