import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from sklearn.cluster import KMeans
from bokeh.charts import Scatter, Histogram, show, defaults
import scipy.ndimage
import pandas as pd
from selenium import webdriver
import urllib.request
from bs4 import BeautifulSoup, Comment
import re

sns.set_style("white")
defaults.width = 430  # for setting size of bokeh plot
defaults.height = 430  # for setting size of bokeh plot


def k_means(dataframe):
    """Cluster the lines of HTML using K-Means and 
    returns the centroids and labels"""
    X = dataframe.as_matrix()
    kmeans = KMeans(n_clusters=3, random_state=0,
                    n_init=30, n_jobs=-1).fit(X)
    centroids = kmeans.cluster_centers_
    labels = kmeans.labels_
    return centroids, labels


def centroid_finder(centroids):
    """Computes the farthest centroids"""
    origin = np.array([0, 0])
    result = {}
    pseudo_centroids = centroids.tolist()
    for value in pseudo_centroids:
        dist = np.linalg.norm(origin - np.array(value))
        result[dist] = value
    sorted_dist = sorted(result.keys(), reverse=True)
    labels_to_consider = []
    for dist in sorted_dist:
        for i, value in enumerate(pseudo_centroids):
            if result[dist] == value:
                labels_to_consider.append(i)
    return labels_to_consider[:2]


def list2txt(some_list, path):
    '''Writes a text file from the input list (into split lines)
    Specify the path as string
    Specify the column names as a list of string '''
    with open(path, "w", encoding="utf-8") as text_file:
        for i, value in enumerate(some_list):
            text_file.write(some_list[i] + "\n")


def plot_scatter_three(data, labels, labels_to_consider):
    """Plot interactive scatter plot using bokeh"""
    status = []
    for i in labels:
        if i in labels_to_consider:
            status.append("Relevant Content")
        else:
            status.append("Irrelevant Content")
    temp = pd.DataFrame({"status": status})
    data = pd.concat([data, temp], axis=1)
    plot_three = Scatter(data, x='derivative', y='smoothed_ttr', title="Smoothed TTR v/s Derivative (Relevance)",xlabel="Derivative of Smoothed TTR", ylabel="Smoothed TTR", color="status", legend='bottom_right')
    return plot_three


def plot_scatter_two(data, labels):
    """Plot interactive scatter plot using bokeh"""
    status = []
    for i in labels:
        status.append("Cluster - " + str(i))
    temp = pd.DataFrame({"status": status})
    data = pd.concat([data, temp], axis=1)
    plot_two = Scatter(data, x='derivative', y='smoothed_ttr', title="Smoothed TTR v/s Derivative (Clusters)",xlabel="Derivative of Smoothed TTR", ylabel="Smoothed TTR", color="status", legend='bottom_right')
    return plot_two


def plot_scatter_one(data):
    """Plot interactive scatter plot using bokeh"""
    status = []
    for i in range(len(data["smoothed_ttr"])):
        status.append("L" + str(i))
    temp = pd.DataFrame({"status": status})
    data = pd.concat([data, temp], axis=1)

    plot_one = Scatter(data, x='derivative', y='smoothed_ttr',
                       title="Smoothed TTR v/s Derivative", xlabel="Derivative of Smoothed TTR", ylabel="Smoothed TTR", legend='bottom_right')
    return plot_one


def plot_hist_one(data):
    """Plot barplot using Bokeh"""
    p = Histogram(data, values='smoothed_ttr', bins=10, title="Smoothed TTR")
    return p


def plot_hist_two(data):
    """Plot barplot using Bokeh"""
    p = Histogram(data, values='derivative', bins=10,
                  title="Derivative of Smoothed TTR")
    return p


def gauss_smoothing(data, sigma):
    """Computes the smoothing if inputted list of data"""
    smoothed = scipy.ndimage.gaussian_filter(data, sigma)
    return smoothed


def getTextToTagRatio(line):
    """Takes a line as input and computes text-length to tag ratio"""
    tag = 0
    text = 0
    i = 0
    while i < len(line):
        if line[i] == '<':
            tag += 1
            beg = i
            try:
                i = line.index('>', beg) + 1
            except ValueError:
                break
        elif tag == 0 and line[i] == '>':
            text = 0
            tag += 1
            i += 1
        else:
            text += 1
            i += 1
    if tag == 0:
        tag = 1
    return text / tag, text, tag


def derivative(input_ttr_list):
    """ Computes the derivative of TTR"""
    deriv = []
    for i in range(len(input_ttr_list) - 1):
        temp = abs(input_ttr_list[i + 1] - input_ttr_list[i])
        deriv.append(temp)
    deriv.append(input_ttr_list[-1])
    print("Derivative of TTR is calculated !")
    return deriv


def get_html_from_url_bs4(url):
    '''Returns text in a URL
    Input is a url
    Output is a list of sentences as given in the page'''
    try:
        try:
            response = urllib.request.urlopen(url)
        except:
            user_agent = {
                'User-Agent': "Mozilla/5.0 (Macintosh; Intel Mac " +
                "OS X 10_9_3) AppleWebKit/537.36 (KHTML, like " +
                "Gecko) Chrome/35.0.1916.47  Safari/537.36"
            }
            req = urllib.request.Request(url, data=None, headers=user_agent)
            response = urllib.request.urlopen(req)
        print("URL is connected")
        html = response.read()
        soup = BeautifulSoup(html, 'lxml')
        # kill all script and style elements
        for script in soup(["style", "meta", "script", "noscript"]):
            script.extract()    # rip it out
        comments = soup.find_all(
            string=lambda text: isinstance(text, Comment))
        for element in comments:
            element.extract()
        pretty_html = soup.prettify()
        return pretty_html
    except Exception as err:
        print(err)
        return 0


def get_html_from_url_sel(url):
    '''Returns text in a URL
    Input is a url
    Output is a list of sentences as given in the page'''
    try:
        driver = webdriver.Firefox()
        driver.get(url)
        body = driver.execute_script("""
           var scripts = document.getElementsByTagName('script');
           var css = document.getElementsByTagName('style');
           var meta = document.getElementsByTagName('meta');
           for(var i = 0,len = scripts.length; i < len; i++)
           {
               scripts[0].parentNode.removeChild(scripts[0]);
           }
           for(var j = 0,len = css.length; j < len; j++)
           {
               css[0].parentNode.removeChild(css[0]);
           }
           for(var j = 0,len = meta.length; j < len; j++)
           {
               meta[0].parentNode.removeChild(meta[0]);
           }
           return document.body.innerHTML
           """)
        driver.quit()
    except:
        return "Selenium error"
    re.sub(r"/<!--[\s\S]*?-->/g", "", body)
    re.sub(r"/<!--\[if IE[0-9]\]-*-*>", "", body)
    return body


def clean_html(html):
    """Cleans the incoming html with basci space normalisation"""
    html_list = html.split("\n")
    html_lines = []
    for line in html_list:
        line = re.sub(r"\s+", " ", line)
        line = line.strip()
        html_lines.append(line)
    html_lines = list(filter(None, html_lines))
    print("HTML Response is cleaned !")
    return html_lines


def get_smoothed_ttr(html_lines):
    ttr_ratios = []
    for index, line in enumerate(html_lines):
        ttr_ratios.append(getTextToTagRatio(line)[0])
    ttr_ratios = gauss_smoothing(ttr_ratios, 1)
    print("Smoothed TTR with Gaussian Smoothing !")
    return ttr_ratios


def index_finder(labels_to_consider, labels):
    """Computes the starting and ending indices of the farthest labels"""
    index_list = []
    for i, value in enumerate(labels):
        if value in labels_to_consider:
            index_list.append(i)
    var1 = index_list[0]
    var2 = index_list[-1]
    return (var1, var2)
