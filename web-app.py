from tornado import web, escape, ioloop
from main import cetr
import os
import json
from bokeh.embed import components
import sys


class MainHandler(web.RequestHandler):

    def get(self, url=None):
        if url is not None:
            self.render("template.html", url=escape.url_unescape(url))
        else:
            self.render("template.html", url=None)

    def post(self):
        # url = self.request.body.decode("utf-8")
        url = self.get_body_argument('url')
        unescaped_url = escape.url_unescape(url)
        scraper = self.get_body_argument('scraper')
        if scraper != "bs4":
            scraper = "sel"
        print("Received '" + unescaped_url)
        try:
            plot_1, plot_2, plot_3, hist_1, hist_2, content = cetr(
                unescaped_url, scraper)
        except:
            e = sys.exc_info()[0]
            self.write(e)
            return
        script, (plot_1_div, plot_2_div, plot_3_div, hist_1_div,
                 hist_2_div) = components((plot_1, plot_2, plot_3,
                                           hist_1, hist_2))
        self.write(json.dumps({
            'script': script,
            'plot_1_div': plot_1_div,
            'plot_2_div': plot_2_div,
            'plot_3_div': plot_3_div,
            'hist_1_div': hist_1_div,
            'hist_2_div': hist_2_div,
            'content': content
        }))


class Application(web.Application):

    def __init__(self):
        handlers = [
            (r"/cetr", MainHandler),
        ]
        settings = {
            "static_path": os.path.join(os.getcwd(), "files")
        }
        web.Application.__init__(self, handlers, **settings)


if __name__ == '__main__':
    app = Application()
    print("Starting app")
    app.listen(8080)
    print("App started on port 8080")
    ioloop.IOLoop.current().start()
